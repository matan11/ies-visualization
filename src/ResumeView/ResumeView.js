import React from 'react';
import _ from 'lodash';
class ResumeView extends React.Component {


    render() {
        return <section className={'cv'}>
            Hello, {this.props.name}
            <Highlighted text="the quick brown fox jumps over the lazy dog"/>
        </section>

    }
}

const Highlighted = ({text = '', highlight = ''}) => {
    if (!highlight.trim()) {
        return <span>{text}</span>
    }
    const regex = new RegExp(`(${_.escapeRegExp(highlight)})`, 'gi')
    const parts = text.split(regex)
    return (
        <span>
        {parts.filter(part => part).map((part, i) => (
            regex.test(part) ? <mark key={i}>{part}</mark> : <span key={i}>{part}</span>
        ))}
    </span>
    )
}
export default ResumeView;