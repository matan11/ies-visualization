import { ForceGraph } from "../components/forceGraph";
import './App.css';
import '../style.css';
import ResumeView from '../ResumeView/ResumeView.js'
import data from '../test_1.json'
import React from 'react';

function App() {
    const nodeHoverTooltip = React.useCallback((node) => {
        return `<div>     
      <b>${node.label}</b>
    </div>`;
    }, []);
  return (
    <div className="App">

        <ResumeView className='left-panel' name='spongeboo'></ResumeView>
        <section className="right-panel">
            <ForceGraph linksData={data.links} nodesData={data.nodes} nodeHoverTooltip={nodeHoverTooltip} />
        </section>
    </div>
  );
}

export default App;
